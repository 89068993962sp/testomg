// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestOMGGameMode.generated.h"

UCLASS(minimalapi)
class ATestOMGGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATestOMGGameMode();
};



